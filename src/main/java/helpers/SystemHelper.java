package helpers;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.base.PageInstance;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.lang.RandomStringUtils;

import java.sql.ResultSet;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class SystemHelper extends PageInstance {
    public static final String URL;
    public static String GUIDUSER="";
    public static Map<String,String> _inputdata;
    public static String MAINWINDOWHANDLER;

    static {
        HashMap<String, String> ARPConfig = read();
        URL = "https://" + ARPConfig.get("lang") + ".wikipedia.org/wiki/";
    }

    public static String getGUIDUSER() {
        return _inputdata.get("GUID");
    }

    public static HashMap<String, String> read() {
        XMLConfiguration config = null;
        HashMap<String, String> map = new HashMap<>();
        try {
            config = new XMLConfiguration("ARP_Configuration.xml");
            map.put("lang", config.getString("lang"));
            return map;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void setImplicitlyWait(int timeout) {
        driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
    }

    public static void Reset_Values() {
        _inputdata = new HashMap<String,String>();
        _inputdata.put("GUID", ("П"+UUID.randomUUID().toString())); // Поставил "П" для того чтобы рандомное отчество начиналось с этой буквы, т.к. тест падает
        _inputdata.put("DATE", new Date().toString());
    }

    public static String GET_VALUE(String value) {
        if (!_inputdata.containsKey(value)) {
            if (value.contains("GUIDUSER")){
                GUIDUSER = String.valueOf("П")+UUID.randomUUID().toString();  // Поставил "П" для того чтобы рандомное отчество начиналось с этой буквы, т.к. тест падает если не совпадают инициалы
                _inputdata.put(value, GUIDUSER);
            }
                //else if(value.toLowerCase().contains("mail"))
                //   _inputdata.put(value,RandomStringUtils.randomAlphanumeric(10)+"@mail.com");
            else if (value.contains("RandMail"))
                _inputdata.put(value, RandomStringUtils.randomAlphanumeric(10) + "@mail.com");
            else if (value.contains("TagRand"))                                   // random value for tag
                _inputdata.put(value, RandomStringUtils.randomNumeric(10));
            else
                _inputdata.put(value, value);
        }
        return _inputdata.get(value);
    }

    public static void waitAppearanceByXpath(WebDriver driver, int delay, int limit, String xpath) throws Exception{
        for (int i=0; i < limit; i++) {
            try {
                WebElement el = driver.findElement(By.xpath(xpath));
                if (el.isDisplayed()) {
                    Thread.sleep(300);
                    return;
                }
            }
            catch (Exception e) {}
            finally {
                Thread.sleep(delay);
            }
        }
        return;
    }

    public static void waitAppearanceByElement(WebDriver driver, int delay, int limit, WebElement el) throws Exception{
        for (int i=0; i < limit; i++) {
            try {
                if (el.isDisplayed()) {
                    Thread.sleep(300);
                    return;
                }
            }
            catch (Exception e) {}
            finally {
                Thread.sleep(delay);
            }
        }
        return;
    }

    public static void RemoveFolderViaSSH(ChannelSftp session, String directory) throws SftpException {
        Vector v = session.ls(directory);
        for (Object e : v) {
            ChannelSftp.LsEntry entry = (ChannelSftp.LsEntry) e;
            entry.getFilename();
            if(entry.getLongname().startsWith("-")){
                session.chmod(511, directory + "/" + entry.getFilename());
                session.rm(directory + "/" + entry.getFilename());
            }
            else{
                RemoveFolderViaSSH(session, directory + "/" + entry.getFilename());
            }
        }
        session.chmod(511, directory);
        session.rmdir(directory);
    }

    public static void RemoveTemporaryProjectFoldersViaSSH(ChannelSftp session, String directory) throws SftpException {
        Vector v = session.ls(directory);
        for (Object e : v) {
            ChannelSftp.LsEntry entry = (ChannelSftp.LsEntry) e;
            Pattern p1 = Pattern.compile("F[0-9][0-9][0-9]");
            Pattern p2 = Pattern.compile("F[0-9][0-9]");
            if(p1.matcher(entry.getFilename()).matches()||p2.matcher(entry.getFilename()).matches()){
                RemoveFolderViaSSH(session, directory + "/" + entry.getFilename());
            }
        }
    }
}