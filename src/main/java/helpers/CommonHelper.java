package helpers;

import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.base.PageInstance;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class CommonHelper extends PageInstance {

    public static String currentArticle = "";
    public static boolean firstRun = true;

    public static boolean navigateToPage(String URL, Element pageElement) throws Exception {
        Thread.sleep(1000);
        if (pageElement==null||!pageElement.isDisplayed()) {
            driver.navigate().to(URL);
            Thread.sleep(1000);
            waitForPageToLoad();
            if(pageElement!=null)
                Waiters.waitAppearanceOf(60, pageElement.getWrappedElement());
            return true;
        } else
            return true;
    }

    public static void waitForPageToLoad(){
        new WebDriverWait(driver, 60).until((ExpectedCondition<Boolean>) wd ->
                ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
    }
}
