package pages.wiki;

import com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.context.annotation.Lazy;
import pages.base.BasePage;

import java.util.List;

public class WikiPage extends BasePage {

    public WikiPage() {
        TypeFactory.containerInitHTMLElements(this);
    }

    @Override
    protected WebElement elementForLoading() throws Exception {
        return wikiLogo.getWrappedElement();
    }

    @Lazy
    @FindBy(xpath = ".//a[@class='mw-wiki-logo']")
    public Link wikiLogo;

    @Lazy
    @FindBy(xpath = ".//input[@id='searchInput']")
    public TextInput searchField;

    @Lazy
    @FindBy(xpath = ".//div[@class='suggestions-results']/a")
    public List<Link> searchAutocompleteLines;

    @Lazy
    @FindBy(xpath = ".//div[@class='suggestions-results']")
    public PlaceHolder searchAutocompleteElement;

    @Lazy
    @FindBy(xpath = ".//div[@id='content']//a[contains(@href, '/wiki/')][not(contains(@href, '/File:'))]")
    public List<Link> articleLinks;

    @Lazy
    @FindBy(xpath = ".//div[@id='p-lang']//li[contains(@class, 'interlanguage')]/a")
    public List<Link> languagesList;
}