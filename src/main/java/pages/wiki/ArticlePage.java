package pages.wiki;

import com.unitedsofthouse.ucucumberpackage.typesfactory.types.*;
import org.openqa.selenium.support.FindBy;
import org.springframework.context.annotation.Lazy;

/**
 * Created by kozlov on 2/20/2017.
 */
public class ArticlePage extends WikiPage {

    @Lazy
    @FindBy(xpath = ".//h1[@id='firstHeading']")
    public Label articleHeader;

    @Lazy
    @FindBy(xpath = ".//p[contains(text(), 'may refer to:')]")
    public Label mayReferTo;
}
