package pages.wiki;

import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Label;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.PlaceHolder;
import org.openqa.selenium.support.FindBy;
import org.springframework.context.annotation.Lazy;

import java.util.List;

/**
 * Created by kozlov on 2/21/2017.
 */
public class SearchPage extends WikiPage {

    @Lazy
    @FindBy(xpath = ".//form[@id='search']")
    public Label searchForm;

    @Lazy
    @FindBy(xpath = ".//ul[@class='mw-search-results']/li")
    public List<PlaceHolder> searchResults;
}
