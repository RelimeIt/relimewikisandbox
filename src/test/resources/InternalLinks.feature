@MainPage
Feature: As user I want to navigate to an article by internal link

  Scenario: As a user I want to be able to navigate to an article by link on Main Page
    Given I navigate to main page
    When I select article link "Powderfinger"
    Then I can see that article "Powderfinger" is opened

  Scenario: As a user I want to be able to navigate to an article by link on another article page
    Given I navigate to article "Ukraine"
    When I select article link "Black Sea"
    Then I can see that article "Black Sea" is opened