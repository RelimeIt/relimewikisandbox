@Search
Feature: As user I want to be able to search for articles

  Background:
    Given I navigate to main page

  Scenario: As a user I want to see autocomplete when I enter search queue
    When I enter "uk" to search field
    Then I can see that all autocomplete lines contain "uk"

  Scenario: As a user I want autocomplete links to navigate me to an article
    When I enter "uk" to search field
    And I select line "Ukraine" in search autocomplete
    Then I can see that article "Ukraine" is opened

  Scenario Outline: As a user I want to navigate to article when I enter exact article name
    When I search "<article>"
    Then I can see that article "<article title>" is opened
    Examples:
    |article|article title|
    |Ukraine|Ukraine     |
    |USA    |United states|
    |Belgium|Belgium      |
    |Java   |Java         |
    |Space  |Space        |



  Scenario: As a user I want to navigate to synonyms page when I enter queue that may refer to multiple subjects
    When I search "Bug"
    Then I can see that synonyms page for "Bug" is opened

  Scenario: As a user I want to navigate to search page when I enter valid queue that has no page
    When I search "something to search for"
    Then I can see that Search page is displayed
    And I can see that search results are displayed