@Multilanguage
Feature: As user I want to be able read articles on different languages

  Background:
    Given I navigate to main page

  Scenario: As a user I want to see list of available version of the Main page
	When I scroll page down to the Languages section
    Then the following languages exist in language list:
      | Simple English |
      | Polski         |
      | Deutsch        |

  Scenario: As a user I want to see list of available version of the article
    When I search "Space"
    Then I can see that article "Space" is opened
    And the following languages exist in language list:
      | Dansk    |
      | Deutsch  |
      | Italiano |
      | Latina   |

  Scenario Outline: As a user I want to navigate to the same article by language link
    When I search "Space"
    Then I can see that article "Space" is opened
    When I change article language to "<language>"
    Then I can see that article "<localized name>" is opened

    Examples:
      | language    | localized name |
      | Deutsch     | Raum (Physik)  |
      | Italiano    | Spazio (fisica)|
      | Latina      | Spatium        |
