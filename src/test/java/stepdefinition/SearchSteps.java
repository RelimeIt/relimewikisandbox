package stepdefinition;

import arp.CucumberArpReport;
import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Link;
import cucumber.api.java.en.*;
import helpers.CommonHelper;
import org.openqa.selenium.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.wiki.ArticlePage;
import pages.wiki.SearchPage;

/**
 * Created by kozlov on 2/21/2017.
 */
public class SearchSteps  extends PageInstance {

    @Autowired
    ArticlePage articlePage;

    @Autowired
    SearchPage searchPage;

    @When("^I enter \"([^\"]*)\" to search field$")
    public void iSearch(String article) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            article = article.replace("current", CommonHelper.currentArticle);
            Waiters.waitAppearanceOf(5, articlePage.searchField.getWrappedElement());
            articlePage.searchField.click();
            articlePage.searchField.clear();
            articlePage.searchField.sendKeys(article);
            ReportService.reportAction("Entered queue '" + article + "' to the search field.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that all autocomplete lines contain \"([^\"]*)\"$")
    public void iSeeAutocompleteLinesContain(String queue) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, articlePage.searchAutocompleteElement.getWrappedElement());
            boolean correct = true;
            queue = queue.toLowerCase();
            for (Link link : articlePage.searchAutocompleteLines) {
                String s = link.getText();
                if(!link.getText().toLowerCase().contains(queue)){
                    correct = false;
                    break;
                }
            }
            ReportService.reportAction("All the autocomplete lines contain '" + queue + "'.", correct);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I select line \"([^\"]*)\" in search autocomplete$")
    public void iSelectLineInAutocomplete(String queue) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, articlePage.searchAutocompleteElement.getWrappedElement());
            boolean correct = false;
            for (Link link : articlePage.searchAutocompleteLines) {
                if(link.getText().contains(queue)){
                    link.click();
                    correct = true;
                    CommonHelper.waitForPageToLoad();
                    break;
                }
            }
            ReportService.reportAction("All the autocomplete lines contain '" + queue + "'.", correct);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I search \"([^\"]*)\"$")
    public void iNavigateToArticleViaSeach(String article) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            article = article.replace("current", CommonHelper.currentArticle);
            Waiters.waitAppearanceOf(5, articlePage.searchField.getWrappedElement());
            articlePage.searchField.click();
            articlePage.searchField.clear();
            articlePage.searchField.sendKeys(article);
            articlePage.searchField.sendKeys(Keys.ENTER);
            CommonHelper.waitForPageToLoad();
            ReportService.reportAction("Searching '" + article + "' queue.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that Search page is displayed$")
    public void iCanSeeThatSearchPageIsDisplayed() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, searchPage.searchForm.getWrappedElement());
            ReportService.reportAction("Search page is displayed.", searchPage.searchForm.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that search results are displayed$")
    public void iCanSeeThatSearchResultsAreDisplayed() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, searchPage.searchForm.getWrappedElement());
            ReportService.reportAction("Search results are displayed.", searchPage.searchResults.size()>0);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
