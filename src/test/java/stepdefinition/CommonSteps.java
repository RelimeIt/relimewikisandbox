package stepdefinition;

import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.*;
import cucumber.api.java.en.*;
import helpers.CommonHelper;
import helpers.SystemHelper;
import arp.CucumberArpReport;
import arp.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.wiki.*;

import java.util.Random;

public class CommonSteps extends PageInstance {

    @Autowired
    ArticlePage articlePage;

    @When("^I navigate to article \"([^\"]*)\"$")
    public void iNavigateToArticle(String article) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            CommonHelper.navigateToPage(SystemHelper.URL + article.replace(" ", "_"), articlePage.articleHeader);
            CommonHelper.currentArticle = article;
            ReportService.reportAction("Navigated to article'" + article + "'.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I select article link \"([^\"]*)\"$")
    public void iSelectLink(String article) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean selected = false;
            if(article.equals("any")){
                Random r = new Random();
                Link link = articlePage.articleLinks.get(r.nextInt(articlePage.articleLinks.size()));
                while(!link.getHref().startsWith(SystemHelper.URL)){
                    link = articlePage.articleLinks.get(r.nextInt(articlePage.articleLinks.size()));
                }
                String value = link.getHref();
                link.click();
                CommonHelper.currentArticle = value.replace(SystemHelper.URL, "").replace("_", " ");
                selected = true;
                CommonHelper.waitForPageToLoad();
            }
            else {
                for (Link link : articlePage.articleLinks) {
                    if (link.getText().equals(article)) {
                        String value = link.getHref();
                        link.click();
                        CommonHelper.currentArticle = value.replace(SystemHelper.URL, "").replace("_", " ");
                        selected = true;
                        CommonHelper.waitForPageToLoad();
                        Thread.sleep(2000);
                        break;
                    }
                }
            }
            ReportService.reportAction("Navigated to article'" + article + "'.", selected);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I navigate to main page$")
    public void iNavigateToMainPage() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            CommonHelper.navigateToPage(SystemHelper.URL + "Main_Page", articlePage.wikiLogo);
            ReportService.reportAction("Navigated to main page.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that article \"([^\"]*)\" is opened$")
    public void iCanSeeThatArticleIsOpened(String article) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            article = article.replace("current", CommonHelper.currentArticle);
            Waiters.waitAppearanceOf(5, articlePage.articleHeader.getWrappedElement());
            ReportService.reportAction("Article '" + article + "' is opened.", articlePage.articleHeader.getText().equals(article));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that synonyms page for \"([^\"]*)\" is opened$")
    public void iCanSeeThatSynonymsPageIsOpened(String article) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, articlePage.mayReferTo.getWrappedElement());
            ReportService.reportAction("Synonyms page for '" + article + "' is opened.", articlePage.mayReferTo.getText().equals(article + " may refer to:"));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I change article language to \"([^\"]*)\"$")
    public void iChangeLanguageTo(String language) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean selected = false;
            for (Link lang : articlePage.languagesList) {
                String s = lang.getText();
                if(lang.getText().toLowerCase().equals(language.toLowerCase())) {
                    lang.click();
                    CommonHelper.waitForPageToLoad();
                    selected = true;
                    break;
                }
            }
            ReportService.reportAction("language was changed to '" + language + "'.", selected);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
